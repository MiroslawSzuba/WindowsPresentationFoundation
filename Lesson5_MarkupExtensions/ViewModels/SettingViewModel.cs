﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson5_MarkupExtensions.ViewModels
{
    public class SettingViewModel
    {
        public object SettingsContent { get; set; }
        public string TitleWindow { get; set; }
    }
}
