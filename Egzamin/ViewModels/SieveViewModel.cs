﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Egzamin.ViewModels
{
    public class SieveViewModel
    {
        private static IEnumerable<int> _primeNumberList;

        public SieveViewModel()
        {
            int minRange = 0;
            int maxRange = 0;

            SieveOfEratosthenes(minRange, maxRange);
        }

        private class KeyValuePair
        {
            public int Key { get; set; }
            public bool Value { get; set; }
        }

        /// <summary>
        /// WIĘCEJ INFORMACJI NA STRONIE https://pl.wikipedia.org/wiki/Sito_Eratostenesa
        /// </summary>
        /// <param name="minRange"></param>
        /// <param name="maxRange"></param>
        private void SieveOfEratosthenes(int minRange, int maxRange) // ZADANIE 1: Pobierz liczby pierwsze z zakresu <2, 1000>.
        {
            var countOfPrimeNumbers = 0;
            var numbers = Enumerable.Range(minRange, maxRange).
                Select(x => new KeyValuePair() { Key = x, Value = true }).
                ToList();

            numbers.ForEach(x =>
            {
                numbers.ForEach(y =>
                {
                    if (y.Key % x.Key == 0 && !(y.Key.Equals(x.Key) && y.Value.Equals(true)))
                    {
                        y.Value = false;
                    }
                });
            });


            Func<KeyValuePair, bool> condition = x => { return true; }; // W tej chwili delegat wyświetli wszystkie liczby z kolekcji.

            _primeNumberList = numbers.
                Where(condition). // ZADANIE 2: Uzupełnij delegat "condition". Delegat ograniczy zbiór tylko do liczb pierwszych z zakresu <400, 800>.
                Take(countOfPrimeNumbers). // ZADANIE 3: Pobierz 24 elementy z powyższego zakresu liczb pierwszych.
                Select(x => x.Key);
        }

        // ZADANIE 4: W klasie MainWindow ustaw DataContext oraz zmień rodzaj wiązania dla kontrolki TextBlock
        // na widoku MainWindow.xaml tak by wyświtelić wynik zadania.
        private string message;
        public string Message
        {
            get
            {
                message = new Exam.ExamResult(_primeNumberList == null ? new List<int>() : _primeNumberList).GetResult();
                return message;
            }
            set
            {
                message = value;
            }
        }
    }
}
