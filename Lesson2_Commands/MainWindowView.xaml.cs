﻿using Lesson2_Commands.ViewModels;
using System.Windows;
using System.Windows.Controls;

namespace Lesson2_Commands
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindowView : Window
    {
        public MainWindowView()
        {
            InitializeComponent();

            var mainViewModel = new MainViewModel();

            DataContext = mainViewModel;
            mMenuPanel.DataContext = new MenuViewModel();
            mConfigurationPanel.DataContext = new TreeViewConfigurationViewModel();
            mExplorerPanel.DataContext = new TreeViewExplorerViewModel(mainViewModel);
        }
    }
}
