﻿using System.ComponentModel;

namespace Lesson2_Commands.ViewModels
{
    public class MainViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged = null;

        virtual protected void OnPropertyChanged(string propName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propName));
        }

        private const string APPLICATION_NAME = "WPF Lessons";
        public string ApplicationName { get { return APPLICATION_NAME; } }

        private string fileContent;
        public string FileContent
        {
            get { return fileContent; }
            set

            {
                if (fileContent != value)
                {
                    fileContent = value;
                    OnPropertyChanged("FileContent");
                }
            }
        }
    }
}
