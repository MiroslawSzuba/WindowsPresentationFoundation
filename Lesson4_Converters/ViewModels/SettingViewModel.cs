﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson4_Converters.ViewModels
{
    public class SettingViewModel
    {
        public object SettingsContent { get; set; }
        public string TitleWindow { get; set; }
    }
}
