﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace Lesson4_Converters.ViewModels
{
    public class FontSettingViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged = null;

        virtual protected void OnPropertyChanged(string propName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propName));
        }

        public FontSettingViewModel(MainViewModel mainViewModel)
        {
            SaveFontCmd = new RelayCommand(x =>
            {
                var label = (Label)x;
                mainViewModel.FontSize = Convert.ToInt32(label.FontSize);
                mainViewModel.FontWeight = label.FontWeight;
                mainViewModel.FontFamily = label.FontFamily;
            });

            CheckedRadioButtonCmd = new RelayCommand(x =>
            {
                var radioButton = (RadioButton)x;
                CheckedRadioButton = radioButton;
            });
        }

        public string TitleWindow { get { return "Font Settings"; } }

        private int fontSize = 8;
        public int FontSize
        {
            get { return fontSize; }
            set

            {
                if (fontSize != value)
                {
                    fontSize = value;
                    OnPropertyChanged("FontSize");
                }
            }
        }

        private int fontWeight = 1;
        public int FontWeight
        {
            get { return fontWeight; }
            set

            {
                if (fontWeight != value)
                {
                    fontWeight = value;
                    OnPropertyChanged("FontWeight");
                }
            }
        }

        public string FontFamily { get; set; } = "Arial";

        private RadioButton checkedRadioButton;
        public RadioButton CheckedRadioButton
        {
            get { return checkedRadioButton; }
            set

            {
                if (checkedRadioButton != value)
                {
                    checkedRadioButton = value;
                    OnPropertyChanged("CheckedRadioButton");
                }
            }
        }

        public ICommand SaveFontCmd { get; set; }
        public ICommand CheckedRadioButtonCmd { get; set; }
    }
}
