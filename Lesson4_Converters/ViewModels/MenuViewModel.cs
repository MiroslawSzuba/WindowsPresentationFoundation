﻿using Lesson4_Converters.UserControls;
using Lesson4_Converters.Views;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace Lesson4_Converters.ViewModels
{
    public class MenuViewModel : List<MenuItem>
    {
        private MainViewModel mainViewModel;

        #region Constants

        private const string FILE = "File";
        private const string NEW = "New";
        private const string OPEN = "Open";
        private const string CLOSE = "Close";
        private const string OPTIONS = "Options";
        private const string FONT = "Font...";
        private const string BACKGROUND = "Backgroud...";
        private const string ZOOM = "Zoom...";
        private const string TOOLBARS = "Toolbars";
        private const string TOOLBOX = "Toolbox";
        private const string NOTIFICATIONS = "Notifications";
        private const string VIEW = "View";
        private const string LAYOUT = "Layout";
        private const string FORMATTING = "Formatting";

        #endregion

        public MenuViewModel(MainViewModel mainViewModel)
        {
            this.mainViewModel = mainViewModel;

            SetCommands();
            CreateMenu();
            SetCommandsBinding(this, null);
        }

        private void SetCommandsBinding(List<MenuItem> listMenuItem, MenuItem currentItem)
        {
            if (currentItem != null)
            {
                foreach(var item in currentItem.Items)
                {
                    var menuItem = (MenuItem)item;
                    if (!menuItem.HasItems)
                    {
                        menuItem.Command = MenuCmd;
                        menuItem.CommandParameter = item;
                    }
                    else
                    {
                        SetCommandsBinding(new List<MenuItem>(), menuItem);
                    }
                }
            }

            foreach(var item in listMenuItem)
            {
                if (!item.HasItems)
                {
                    item.Command = MenuCmd;
                    item.CommandParameter = item;
                }
                else
                {
                    SetCommandsBinding(new List<MenuItem>(), item);
                }
            }
        }

        private void CreateMenu()
        {
            var fileMenuItem = new MenuItem() { Header = FILE };
            fileMenuItem.ItemsSource = new List<MenuItem>
            {
                new MenuItem(){ Header = NEW},
                new MenuItem(){ Header = OPEN},
                new MenuItem(){ Header = CLOSE}
            };

            var optionsMenuItem = new MenuItem() { Header = OPTIONS };
            optionsMenuItem.ItemsSource = new List<MenuItem>
            {
                new MenuItem() { Header = FONT},
                new MenuItem(){ Header = BACKGROUND},
                new MenuItem(){ Header = ZOOM}
            };

            var toolbarsMenuItem = new MenuItem() { Header = TOOLBARS };
            toolbarsMenuItem.ItemsSource = new List<MenuItem>
            {
                new MenuItem(){ Header = TOOLBOX},
                new MenuItem(){ Header = NOTIFICATIONS}
            };

            var viewMenuItem = new MenuItem() { Header = VIEW };
            viewMenuItem.ItemsSource = new List<MenuItem>
            {
                new MenuItem(){ Header = LAYOUT},
                new MenuItem(){ Header = FORMATTING},
                toolbarsMenuItem
            };

            Add(fileMenuItem);
            Add(optionsMenuItem);
            Add(viewMenuItem);
        }

        private void SetCommands()
        {

            MenuCmd = new RelayCommand(x =>
            {
                var item = (MenuItem)x;
                UserControl userControl;

                if (item.HasHeader && !item.HasItems)
                {
                    if (item.Header.Equals(FONT))
                    {
                        userControl = new FontSettingUserControl(mainViewModel);
                    }
                    else if (item.Header.Equals(BACKGROUND))
                    {
                        userControl = new BackgroundSettingUserControl(mainViewModel);
                    }
                    else
                    {
                        userControl = new UserControl();
                    }

                    var settingWindow = new SettingWindow(userControl);
                    settingWindow.ResizeMode = ResizeMode.NoResize;
                    settingWindow.ShowDialog();
                }
            });

        }

        public ICommand MenuCmd { get; set; }
    }
}
