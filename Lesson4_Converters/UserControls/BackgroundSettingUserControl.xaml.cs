﻿using Lesson4_Converters.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Lesson4_Converters.UserControls
{
    /// <summary>
    /// Interaction logic for BackgroundSettingUserControl.xaml
    /// </summary>
    public partial class BackgroundSettingUserControl : UserControl
    {
        public BackgroundSettingUserControl(MainViewModel mainViewModel)
        {
            InitializeComponent();

            var backgroundSettingViewModel = new BackgroundSettingViewModel(mainViewModel);
            DataContext = backgroundSettingViewModel;
            Tag = backgroundSettingViewModel.TitleWindow;
        }
    }
}
