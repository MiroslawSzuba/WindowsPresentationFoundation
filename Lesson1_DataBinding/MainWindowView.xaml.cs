﻿using Lesson1_DataBinding.ViewModels;
using System.Windows;
using System.Windows.Controls;

namespace Lesson1_DataBinding
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindowView : Window
    {
        private TreeViewExplorerViewModel treeViewExplorerViewModel;

        public MainWindowView()
        {
            InitializeComponent();

            var mainViewModel = new MainViewModel();

            DataContext = mainViewModel;
            mMenuPanel.DataContext = new MenuViewModel();
            mConfigurationPanel.DataContext = new TreeViewConfigurationViewModel();
            treeViewExplorerViewModel = new TreeViewExplorerViewModel(mainViewModel);
            mExplorerPanel.DataContext = treeViewExplorerViewModel;
        }

        private void TreeView_SelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            treeViewExplorerViewModel.SelectedItem = (sender as TreeView)?.SelectedItem as TreeViewItem;
        }
    }
}
