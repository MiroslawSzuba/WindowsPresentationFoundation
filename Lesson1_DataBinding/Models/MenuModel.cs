﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace Lesson1_DataBinding.Models
{
    public class MenuModel
    {
        public string ItemName { get; set; }
        public Image ItemIcon { get; set; }
        public string Key { get; set; }
    }
}
