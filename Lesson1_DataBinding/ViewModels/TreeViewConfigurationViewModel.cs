﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace Lesson1_DataBinding.ViewModels
{
    public class TreeViewConfigurationViewModel : List<TreeViewItem>
    {
        #region Constants

        private const string FILE = "File";
        private const string NEW = "New";
        private const string OPEN = "Open";
        private const string CLOSE = "Close";
        private const string OPTIONS = "Options";
        private const string FONT = "Font...";
        private const string BACKGROUND = "Backgroud...";
        private const string ZOOM = "Zoom...";
        private const string TOOLBARS = "Toolbars";
        private const string TOOLBOX = "Toolbox";
        private const string NOTIFICATIONS = "Notifications";
        private const string VIEW = "View";
        private const string LAYOUT = "Layout";
        private const string FORMATTING = "Formatting";

        #endregion

        public TreeViewConfigurationViewModel()
        {
            var fileTreeViewItem = new TreeViewItem() { Header = FILE, IsExpanded = true };
            fileTreeViewItem.ItemsSource = new List<TreeViewItem>
            {
                new TreeViewItem(){ Header = NEW},
                new TreeViewItem(){ Header = OPEN},
                new TreeViewItem(){ Header = CLOSE}
            };

            var optionsTreeViewItem = new TreeViewItem() { Header = OPTIONS, IsExpanded = true };
            optionsTreeViewItem.ItemsSource = new List<TreeViewItem>
            {
                new TreeViewItem(){ Header = FONT},
                new TreeViewItem(){ Header = BACKGROUND},
                new TreeViewItem(){ Header = ZOOM}
            };

            var toolbarsTreeViewItem = new TreeViewItem() { Header = TOOLBARS, IsExpanded = true };
            toolbarsTreeViewItem.ItemsSource = new List<TreeViewItem>
            {
                new TreeViewItem(){ Header = TOOLBOX},
                new TreeViewItem(){ Header = NOTIFICATIONS}
            };

            var viewTreeViewItem = new TreeViewItem() { Header = VIEW, IsExpanded = true };
            viewTreeViewItem.ItemsSource = new List<TreeViewItem>
            {
                new TreeViewItem(){ Header = LAYOUT},
                new TreeViewItem(){ Header = FORMATTING},
                toolbarsTreeViewItem
            };

            Add(fileTreeViewItem);
            Add(optionsTreeViewItem);
            Add(viewTreeViewItem);
        }
    }
}
