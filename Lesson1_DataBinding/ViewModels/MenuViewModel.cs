﻿using System.Collections.Generic;
using System.Windows.Controls;

namespace Lesson1_DataBinding.ViewModels
{
    public class MenuViewModel : List<MenuItem>
    {
        #region Constants

        private const string FILE = "File";
        private const string NEW = "New";
        private const string OPEN = "Open";
        private const string CLOSE = "Close";
        private const string OPTIONS = "Options";
        private const string FONT = "Font...";
        private const string BACKGROUND = "Backgroud...";
        private const string ZOOM = "Zoom...";
        private const string TOOLBARS = "Toolbars";
        private const string TOOLBOX = "Toolbox";
        private const string NOTIFICATIONS = "Notifications";
        private const string VIEW = "View";
        private const string LAYOUT = "Layout";
        private const string FORMATTING = "Formatting";

        #endregion

        public MenuViewModel()
        {
            var fileMenuItem = new MenuItem() { Header = FILE };
            fileMenuItem.ItemsSource = new List<MenuItem>
            {
                new MenuItem(){ Header = NEW},
                new MenuItem(){ Header = OPEN},
                new MenuItem(){ Header = CLOSE}
            };

            var optionsMenuItem = new MenuItem() { Header = OPTIONS };
            optionsMenuItem.ItemsSource = new List<MenuItem>
            {
                new MenuItem(){ Header = FONT},
                new MenuItem(){ Header = BACKGROUND},
                new MenuItem(){ Header = ZOOM}
            };

            var toolbarsMenuItem = new MenuItem() { Header = TOOLBARS };
            toolbarsMenuItem.ItemsSource = new List<MenuItem>
            {
                new MenuItem(){ Header = TOOLBOX},
                new MenuItem(){ Header = NOTIFICATIONS}
            };

            var viewMenuItem = new MenuItem() { Header = VIEW };
            viewMenuItem.ItemsSource = new List<MenuItem>
            {
                new MenuItem(){ Header = LAYOUT},
                new MenuItem(){ Header = FORMATTING},
                toolbarsMenuItem
            };

            Add(fileMenuItem);
            Add(optionsMenuItem);
            Add(viewMenuItem);
        }
    }
}
