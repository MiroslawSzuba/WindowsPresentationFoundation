﻿using System.Windows.Input;
using System.Windows.Media;

namespace Lesson5_StylesTriggers.ViewModels
{
    public class BackgroundSettingViewModel
    {
        public BackgroundSettingViewModel(MainViewModel mainViewModel)
        {
            SaveBackgroundCmd = new RelayCommand(x =>
            {
                mainViewModel.BackgroundWindow = (SolidColorBrush)x;
            });
        }

        public string TitleWindow { get { return "Background Settings"; } }
        public int RedColorIntValue { get; set; } = 0;
        public int GreenColorIntValue { get; set; } = 0;
        public int BlueColorIntValue { get; set; } = 0;
        public ICommand SaveBackgroundCmd { get; set; }
    }
}
