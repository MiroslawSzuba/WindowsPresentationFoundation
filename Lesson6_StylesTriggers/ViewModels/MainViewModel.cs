﻿using System.ComponentModel;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;

namespace Lesson5_StylesTriggers.ViewModels
{
    public class MainViewModel : INotifyPropertyChanged
    {
        public MainViewModel()
        {
            LoadUsersFromXmlCmd = new RelayCommand(x => {
                FileContent = x.ToString();
            });
        }

        public event PropertyChangedEventHandler PropertyChanged = null;

        virtual protected void OnPropertyChanged(string propName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propName));
        }

        private const string APPLICATION_NAME = "WPF Lessons";
        public string ApplicationName { get { return APPLICATION_NAME; } }

        private string fileContent;
        public string FileContent
        {
            get { return fileContent; }
            set

            {
                if (fileContent != value)
                {
                    fileContent = value;
                    OnPropertyChanged("FileContent");
                }
            }
        }

        private SolidColorBrush backgroundWindow = Brushes.LightSkyBlue;
        public SolidColorBrush BackgroundWindow
        {
            get { return backgroundWindow; }
            set

            {
                if (backgroundWindow != value)
                {
                    backgroundWindow = value;
                    OnPropertyChanged("BackgroundWindow");
                }
            }
        }

        private int fontSize = 12;
        public int FontSize
        {
            get { return fontSize; }
            set

            {
                if (fontSize != value)
                {
                    fontSize = value;
                    OnPropertyChanged("FontSize");
                }
            }
        }

        private FontWeight fontWeight = FontWeight.FromOpenTypeWeight(1);
        public FontWeight FontWeight
        {
            get { return fontWeight; }
            set

            {
                if (fontWeight != value)
                {
                    fontWeight = value;
                    OnPropertyChanged("FontWeight");
                }
            }
        }

        private FontFamily fontFamily = new FontFamily("Arial");
        public FontFamily FontFamily
        {
            get { return fontFamily; }
            set

            {
                if (fontFamily != value)
                {
                    fontFamily = value;
                    OnPropertyChanged("FontFamily");
                }
            }
        }

        public ICommand LoadUsersFromXmlCmd { get; set; }
    }
}
