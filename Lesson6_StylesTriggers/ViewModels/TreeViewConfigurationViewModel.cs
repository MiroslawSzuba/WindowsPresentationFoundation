﻿using Lesson5_StylesTriggers.UserControls;
using Lesson5_StylesTriggers.Views;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace Lesson5_StylesTriggers.ViewModels
{
    public class TreeViewConfigurationViewModel : List<TreeViewItem>
    {
        private readonly MainViewModel mainViewModel;

        #region Constants

        private const string FILE = "File";
        private const string NEW = "New";
        private const string OPEN = "Open";
        private const string CLOSE = "Close";
        private const string OPTIONS = "Options";
        private const string FONT = "Font...";
        private const string BACKGROUND = "Backgroud...";
        private const string ZOOM = "Zoom...";
        private const string TOOLBARS = "Toolbars";
        private const string TOOLBOX = "Toolbox";
        private const string NOTIFICATIONS = "Notifications";
        private const string VIEW = "View";
        private const string LAYOUT = "Layout";
        private const string FORMATTING = "Formatting";

        #endregion

        public TreeViewConfigurationViewModel(MainViewModel mainViewModel)
        {
            this.mainViewModel = mainViewModel;

            SetCommands();

            CreateConfigurationTreeView();
        }

        private void CreateConfigurationTreeView()
        {
            var fileTreeViewItem = new TreeViewItem() { Header = FILE, IsExpanded = true };
            fileTreeViewItem.ItemsSource = new List<TreeViewItem>
            {
                new TreeViewItem(){ Header = NEW},
                new TreeViewItem(){ Header = OPEN},
                new TreeViewItem(){ Header = CLOSE}
            };

            var optionsTreeViewItem = new TreeViewItem() { Header = OPTIONS, IsExpanded = true };
            optionsTreeViewItem.ItemsSource = new List<TreeViewItem>
            {
                new TreeViewItem(){ Header = FONT},
                new TreeViewItem(){ Header = BACKGROUND},
                new TreeViewItem(){ Header = ZOOM}
            };

            var toolbarsTreeViewItem = new TreeViewItem() { Header = TOOLBARS, IsExpanded = true };
            toolbarsTreeViewItem.ItemsSource = new List<TreeViewItem>
            {
                new TreeViewItem(){ Header = TOOLBOX},
                new TreeViewItem(){ Header = NOTIFICATIONS}
            };

            var viewTreeViewItem = new TreeViewItem() { Header = VIEW, IsExpanded = true };
            viewTreeViewItem.ItemsSource = new List<TreeViewItem>
            {
                new TreeViewItem(){ Header = LAYOUT},
                new TreeViewItem(){ Header = FORMATTING},
                toolbarsTreeViewItem
            };

            Add(fileTreeViewItem);
            Add(optionsTreeViewItem);
            Add(viewTreeViewItem);
        }

        private void SetCommands()
        {

            ConfigurationCmd = new RelayCommand(x =>
            {
                if (!(x is TreeViewItem))
                {
                    return;
                }

                var item = (TreeViewItem)x;
                UserControl userControl;

                if (item.HasHeader && !item.HasItems)
                {
                    if (item.Header.Equals(FONT))
                    {
                        userControl = new FontSettingUserControl(mainViewModel);
                    }
                    else if (item.Header.Equals(BACKGROUND))
                    {
                        userControl = new BackgroundSettingUserControl(mainViewModel);
                    }
                    else
                    {
                        userControl = new UserControl();
                    }

                    var settingWindow = new SettingWindow(userControl);
                    settingWindow.ResizeMode = ResizeMode.NoResize;
                    settingWindow.ShowDialog();
                }
            });

        }

        public ICommand ConfigurationCmd { get; set; }
    }
}
