﻿using Lesson5_StylesTriggers.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Lesson5_StylesTriggers.Views
{
    /// <summary>
    /// Interaction logic for SettingWindow.xaml
    /// </summary>
    public partial class SettingWindow : Window
    {
        public SettingWindow(UserControl usercontrol)
        {
            InitializeComponent();

            var settingViewModel = new SettingViewModel();
            settingViewModel.TitleWindow = usercontrol?.Tag?.ToString() == null ? "Title" : usercontrol?.Tag?.ToString();
            settingViewModel.SettingsContent = usercontrol;

            DataContext = settingViewModel;
        }
    }
}
