﻿using Lesson5_StylesTriggers.ViewModels;
using System.Windows;
using System.Windows.Controls;

namespace Lesson5_StylesTriggers
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindowView : Window
    {
        public MainWindowView()
        {
            InitializeComponent();

            var mainViewModel = new MainViewModel();

            DataContext = mainViewModel;
            mMenuPanel.DataContext = new MenuViewModel(mainViewModel);
            mConfigurationPanel.DataContext = new TreeViewConfigurationViewModel(mainViewModel);
            mExplorerPanel.DataContext = new TreeViewExplorerViewModel(mainViewModel);
        }
    }
}
