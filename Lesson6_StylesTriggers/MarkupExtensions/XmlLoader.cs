﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Markup;
using System.Xml.Linq;

namespace Lesson5_StylesTriggers.MarkupExtensions
{
    public class XmlLoader : MarkupExtension
    {
        public string Source { get; set; }
        public string Path { get; set; }

        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            if (String.IsNullOrEmpty(Source))
            {
                throw new Exception("Source cannot be Null.");
            }
            else if (String.IsNullOrEmpty(Path))
            {
                throw new Exception("Path cannot be Null.");
            }
            else
            {
                XDocument doc = XDocument.Load(Source);
                IEnumerable<XElement> list = doc.Descendants(Path);
                return String.Concat(list.Select(x => x.Value).ToList());
            }
        }
    }
}
