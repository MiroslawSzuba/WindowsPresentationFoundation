﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace Lesson3_ValidationRules.ViewModels
{
    public class FontSettingViewModel
    {
        public string TitleWindow { get { return "Font Settings"; } }
        public int FontSize { get; set; }
        public int FontWeight { get; set; }
        public FontFamily FontFamily { get; set; } 
    }
}
