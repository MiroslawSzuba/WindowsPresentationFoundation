﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Controls;
using System.Windows.Input;

namespace Lesson3_ValidationRules.ViewModels
{
    public class TreeViewExplorerViewModel : List<TreeViewItem>
    {
        private readonly MainViewModel mainViewModel;

        public TreeViewExplorerViewModel(MainViewModel mainViewModel)
        {
            SetCommands();

            string rootPath = Path.GetDirectoryName(Path.GetDirectoryName(Path.GetDirectoryName(Directory.GetCurrentDirectory())));
            var rootDirInfo = new DirectoryInfo(rootPath);
            var rootTreeViewItem = new TreeViewItem() { Header = rootDirInfo.Name, Tag = rootDirInfo };

            this.mainViewModel = mainViewModel;
            rootTreeViewItem.IsExpanded = true;

            // Add root to the tree view.
            Add(rootTreeViewItem);
            FillTreeViewExplorer(new DirectoryInfo(rootPath), rootTreeViewItem);
        }

        private void SetCommands()
        {
            SelectedItemCmd = new RelayCommand(x => {
                SelectedItem = x as TreeViewItem;
            });
        }

        /// <summary>
        /// Fill tree view structure.
        /// </summary>
        /// <param name="sDirInfo">Stores information about current directory.</param>
        /// <param name="currentItem">Stores information about current item.</param>
        private void FillTreeViewExplorer(DirectoryInfo sDirInfo, TreeViewItem currentItem)
        {
            try
            {
                foreach (string d in Directory.GetDirectories(sDirInfo.FullName))
                {
                    var subDirInfo = new DirectoryInfo(d);
                    var dirItem = new TreeViewItem() { Header = subDirInfo.Name, Tag = subDirInfo };

                    // Add items (directories) to the parent object.
                    currentItem.Items.Add(dirItem);

                    foreach (string f in Directory.GetFiles(d))
                    {
                        var fileInfo = new FileInfo(f);
                        var fileItem = new TreeViewItem() { Header = fileInfo.Name, Tag = fileInfo };

                        // Add items (files) to the parent object.
                        dirItem.Items.Add(fileItem);
                    }

                    // Again call this method to check if current directory has a children.
                    FillTreeViewExplorer(subDirInfo, dirItem);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private TreeViewItem selectedItem;
        public TreeViewItem SelectedItem
        {
            set
            {
                if (selectedItem != value &&
                    value != null &&
                    value.Tag is FileInfo) // Checks if file on the tree view has been selected.
                {
                    selectedItem = value;

                    FileInfo fileInfo = new FileInfo(selectedItem.Tag.ToString()); // Get file info basing on path to file.
                    mainViewModel.FileContent = fileInfo.OpenText().ReadToEnd(); // Read content of file and set it to FileContent property in MainViewModel.
                }
            }
        }

        public ICommand SelectedItemCmd { get; set; }
    }
}
