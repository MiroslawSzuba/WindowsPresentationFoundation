﻿using System;
using System.Globalization;
using System.Text.RegularExpressions;
using System.Windows.Controls;

namespace Lesson3_ValidationRules.ValidationRules
{
    public class RegExpValidationRule : ValidationRule
    {
        private string _pattern;
        private Regex _regExp;

        public string Message { get; set; } = String.Empty;

        public string Pattern
        {
            get
            {
                return _pattern;
            }
            set
            {
                _pattern = value;
                _regExp = new Regex(_pattern, RegexOptions.IgnoreCase);
            }
        }

        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            if (_regExp.Match(value?.ToString()).Success)
            {
                return new ValidationResult(true, null);
            }
            else
            {
                return new ValidationResult(false, Message);
            }
        }
    }
}
