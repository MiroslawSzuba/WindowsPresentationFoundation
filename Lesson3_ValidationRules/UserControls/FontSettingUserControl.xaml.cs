﻿using Lesson3_ValidationRules.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Lesson3_ValidationRules.UserControls
{
    /// <summary>
    /// Interaction logic for FontSettingUserControl.xaml
    /// </summary>
    public partial class FontSettingUserControl : UserControl
    {
        public FontSettingUserControl()
        {
            InitializeComponent();

            var fontSettingViewModel = new FontSettingViewModel();
            DataContext = fontSettingViewModel;
            Tag = fontSettingViewModel.TitleWindow;
        }
    }
}
